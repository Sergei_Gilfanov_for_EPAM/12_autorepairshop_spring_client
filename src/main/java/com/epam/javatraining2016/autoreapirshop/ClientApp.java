package com.epam.javatraining2016.autoreapirshop;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopServiceEndpoint;
import tag.sergei_gilfanov_epam_com_2016_06_27.autorepairshop.Order;

@Component
public class ClientApp {
  private static final Logger log = LoggerFactory.getLogger(ClientApp.class);

  @Autowired
  private AutoRepairShopServiceEndpoint service;

  public String getVersion() {
    return service.getVersion();
  }

  public void run() {
    int client1Id = service.createClient("Первый клиент");
    int client2Id = service.createClient("Второй клиент");

    service.createOrder(client1Id);
    service.createOrder(client1Id);
    service.createOrder(client2Id);

    // ****

    List<Order> client1Orders = service.getOrders(client1Id);
    List<Order> client2Orders = service.getOrders(client1Id);

    service.changeOrderStatus(client1Orders.get(0).getId(), "waiting");
    service.changeOrderStatus(client2Orders.get(0).getId(), "in progress");
    service.changeOrderStatus(client1Orders.get(0).getId(), "done");

    // ****

    client1Orders = service.getOrders(client1Id);
    client2Orders = service.getOrders(client2Id);
    for (Order order : client1Orders) {
      log.info("Client 1 order: {} - {}", order.getId(), order.getStatus());
    }

    for (Order order : client2Orders) {
      log.info("Client 2 order: {} - {}", order.getId(), order.getStatus());
    }
  }
}

package com.epam.javatraining2016.autoreapirshop;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;

import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopServiceEndpoint;

public class Main {

  @Bean
  AutoRepairShopService autoRepairShopService() {
    return new AutoRepairShopService();
  }

  @Bean
  AutoRepairShopServiceEndpoint autoRepairShopServiceEndpoint(
      AutoRepairShopService accountService) {
    return accountService.getAutoRepairShopServiceEndpointPort();
  }

  @Bean(initMethod = "run")
  ClientApp client() {
    return new ClientApp();
  }

  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
  }
}
